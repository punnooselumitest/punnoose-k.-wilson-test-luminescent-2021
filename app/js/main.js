$(function() {  
    $.ajax({
    url: '../src/data/products.json',
    dataType: 'json',
        success: function(data) {

            $.each(data, function(key, val) {
                var favClass = (val.isFav == true) ? 'plp__item__isFav--fav' : '';
                var ratingString = '';
                for(i=0; i<val.rating; i++) {
                    ratingString = ratingString + '<li></li>'
                }
                var oldPriceString = (val.oldPrice != null) ? "<span class='plp-old-price'>"+val.oldPrice+"</span>" : '';
                var savingsString = (val.savings != null) ? "<span class='plp-save-price'>You Save "+val.savings+"</span>" : '';
                
                var markupString = "<div class='plp__item'>"+
                "<div class='plp__item__figure'><button class='plp__item__isFav "+favClass+"'></button>"+
                "<figure><a href='"+val.url+"'><img src='"+val.picture+"' alt='"+val.name+"'></img></a></figure></div>"+
                "<div class='plp__item__details'><div class='details-top'>"+
                "<h3 class='plp-title'><a href='"+val.url+"'>"+val.name+"</a></h3>"+
                "<span class='plp-size'>"+val.size+"</span>"+
                "<ul class='plp-rating'>"+ratingString+"</ul></div>"+
                "<div class='details-bottom'><span class='plp-price'>"+val.price+"</span>"+
                oldPriceString+
                savingsString+
                "</div></div></div>";

                $('.plp__items-wrap').append(markupString);
            });
            
        },
        statusCode: {
            404: function() {
                alert('There was a problem with the server.  Try again soon!');
            }
        }
    });

    // Layout Toggle 
    $('.jsLayoutToggle').on('click', function () {
        var layout = $(this).attr('data-layout');
        $('.jsLayoutToggle').removeClass('active');
        $(this).addClass('active');
        if(layout == 'list') {
            $('.plp__items-wrap').addClass('plp__items-wrap--list');
        } else {
            $('.plp__items-wrap').removeClass('plp__items-wrap--list');
        }
    })

  });