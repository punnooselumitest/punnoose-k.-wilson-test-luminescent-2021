const form = document.querySelector('#form');
const theName = document.querySelector('#name');
const nameError = document.querySelector('#nameError');
const surname = document.querySelector('#surname');
const surnameError = document.querySelector('#surnameError');
const email = document.querySelector('#email');
const emailError = document.querySelector('#emailError');
const phone = document.querySelector('#phone');
const phoneError = document.querySelector('#phoneError');
const password = document.querySelector('#password');

function validateCaseSensitive (string) {
    if (string === string.toUpperCase() || string === string.toLowerCase()) {
        return true
    } else {
        return false
    }
}

function validateEmail(emailVal) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(emailVal).toLowerCase());
}


form.addEventListener('submit', (error) => {
    var errorFlag = false;
    error.preventDefault();
    if (theName.value == '') {
        nameError.textContent = "it's a required field"
        errorFlag = true;
    } else {
        if(!validateCaseSensitive (theName.value)) {
            nameError.textContent = "no case sensitive"
            errorFlag = true;
        } else {
            nameError.textContent = ""
        }
    }
    if (surname.value == '') {
        surnameError.textContent = "it's a required field"
        errorFlag = true;
    } else {
        if(!validateCaseSensitive (surname.value)) {
            surnameError.textContent = "no case sensitive"
            errorFlag = true;
        } else {
            surnameError.textContent = ""
        }
    }

    if (email.value == '') {
        emailError.textContent = "it's a required field"
    } else {
        if(!validateEmail(email)) {
            emailError.textContent = "invalid email"
            errorFlag = true;
        } else {
            emailError.textContent = ""
        }
    }
    if (phone.value == '') {
        phoneError.textContent = "it's a required field"
        errorFlag = true;
    } else {
        phoneError.textContent = ""
    }

    if (password.value == '') {
        passwordError.textContent = "it's a required field"
        errorFlag = true;
    } else {
        passwordError.textContent = ""
    }

    if (errorFlag) {
        error.preventDefault();
    }
})